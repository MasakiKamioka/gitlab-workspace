# 徹底的に解説！】Djangoの基礎をマスターして、3つのアプリを作ろう！（Django2版 / 3版を同時公開中です） [Udemy]

## 環境構築コマンド(venvを使用)
- 対象のディレクトリに移動
- `brew install python3-venv`   
- `python3 -m venv venv`   
- `source venv/bin/activate`   
- `pip install django`
- `django-admin startproject helloworld`

## LANサーバ立ち上げ
- `cd helloworld`
- `python manage.py runserver`

## アプリケーション作成
- `python manage.py startapp helloworldapp`

### venvを使用
- `source venv/bin/activate`   

### venvからチェックアウト
- `deactivate`

