from pathlib import Path
from django.http import HttpResponse
from django.views.generic import TemplateView


def helloworld_func(req):
    res = HttpResponse('<h1>Hello world</h1>')
    return res


def some_view(req):
    print(Path(__file__))
    return HttpResponse('')


class Helloworld_class(TemplateView):
    template_name = 'hello.html'
